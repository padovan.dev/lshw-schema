# lshw-schema

Java classes to manage `lshw` command output

Xsd schema of the `lshw` command output has been copied from here:

[https://ezix.org/src/pkg/lshw/src/branch/master/docs/lshw.xsd](https://ezix.org/src/pkg/lshw/src/branch/master/docs/lshw.xsd)

## Use
[Maven repository](https://gitlab.com/padovan.dev/lshw-schema/-/packages)